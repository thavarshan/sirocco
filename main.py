# Module Imports
from flask import Flask, render_template, redirect, url_for, request
from validator import validate_email
from flask_sqlalchemy import SQLAlchemy
from mailer import send_email

# Application Bootstrap & Initialization
app = Flask(__name__)

# Database Configurations
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://naoybbclcndirh:7be3f2ae73ba149c6184b19f7a7c8e45d09fb13761211036665d686a4e81f5c0@ec2-52-20-248-222.compute-1.amazonaws.com:5432/dfddr6a9p6l4j4'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

# Email Model
class Email(db.Model):
    __tablename__ = 'emails'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(200), unique=True)

    def __init__(self, email):
        self.email = email


# Application Routes
@app.route('/')
def index():
   return render_template('index.html')

@app.route('/success')
def success():
   return render_template('success.html')

@app.route('/submit', methods=['POST'])
def submit():
    if request.method == 'POST':
        email = request.form['email']

    if email == None or not validate_email(email):
        return render_template('index.html', message='Please provide a valide email address')

    if db.session.query(Email).filter(Email.email == email).count() != 0:
        return render_template('index.html', message='You have already subscribed to our mailing list')

    db.session.add(Email(email))
    db.session.commit()

    send_email(email)

    return render_template('success.html')

# Run Application
if __name__ == '__main__':
    app.debug = True
    app.run()
