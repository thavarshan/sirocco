import smtplib
from email.mime.text import MIMEText


def send_email(email):
    # Send email to customer
    port = 2525
    smtp_servcer = 'smtp.mailtrap.io'
    login = 'getyourown'
    password = ''
    sender_email = 'thegoodpeople@sirocco.com'
    message = f"<h3>Thanks again!</h3><p>Thanks again for signing up to our newsletter.</p>"
    msg = MIMEText(message, 'html')
    msg['Subject'] = 'Sirocco Newsletter'
    msg['From'] = sender_email
    msg['To'] = email

    # Send email
    with smtplib.SMTP(smtp_servcer, port) as server:
        server.login(login, password)
        server.sendmail(sender_email, email, msg.as_string())
